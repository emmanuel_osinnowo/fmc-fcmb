angular.module('main', ['ionic', 'main.controller', 'main.services', 'ngCordova'])
.config(function($stateProvider, $urlRouterProvider, $httpProvider){
	$httpProvider.defaults.timeout = 30000;

	$stateProvider
		.state('app', {
			url: '/menu',
			abstract: true,
			templateUrl: "templates/menu.html",
			controller: "sideMenuCtrl"
		})
		.state('app.splash', {
			url: '/splash',
			views: {
				'menuContent':{
					templateUrl: "templates/splash.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.terms', {
			url: "/terms", 
			views : {
				'menuContent':{
					templateUrl: "templates/terms.html",
					controller: "registerCtrl"
				}
			}
		})
		.state('app.forgot', {
			url: "/forgot",
			views: {
				'menuContent' : {
					templateUrl: "templates/forgot.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.bankingid',{
			url: "/bankingid",
			views:{
				'menuContent':{
					templateUrl: "templates/bankingid.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.password',{
			url: "/password",
			views:{
				'menuContent':{
					templateUrl: "templates/password.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.notifications',{
			url: "/notifications",
			views:{
				'menuContent':{
					templateUrl: "templates/notifications.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.contact',{
			url: "/contact",
			views:{
				'menuContent':{
					templateUrl: "templates/contact.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.faqs',{
			url: "/faqs",
			views:{
				'menuContent':{
					templateUrl: "templates/faqs.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.branches',{
			url: "/branches",
			views:{
				'menuContent':{
					templateUrl: "templates/branches.html",
					controller: "mapCtrl"
				}
			}
		})
		.state('app.register-select', {
			url: "/register-select",
			views:{
				'menuContent':{
					templateUrl: "templates/register-select.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.product-services', {
			url : "/product-services",
			views:{
				'menuContent' : {
					templateUrl : "templates/product-services.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.product-services-choose',{
			url: "/product-services/:ps",
			views: {
				'menuContent' :{
					templateUrl : "templates/product-services-choose.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.card', {
			url: "/card",
			views: {
				'menuContent': {
					templateUrl: "templates/card.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.internet-banking', {
			url: "/internet-banking",
			views: {
				'menuContent': {
					templateUrl: "templates/internet-banking.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.bank-account', {
			url: "/bank-account",
			views: {
				'menuContent': {
					templateUrl: "templates/bank-account.html",
					controller : "mainCtrl"
				}
			}
		})
		.state('app.fmc-registration', {
			url: "/fmc-registration",
			views: {
				'menuContent': {
					templateUrl: "templates/fmc-registration.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.main', {
			url: "/main/:m_balance/:m_id/:m_first_name/:m_last_name/:m_phone_number/:m_email",
			views: {
				'menuContent': {
					templateUrl: "templates/main.html",
					controller: "sideMenuCtrl"
				}
			}
		})
		.state('app.fund-transfer', {
			url: "/fund-transfer/:m_phone_number",
			views: {
				'menuContent': {
					templateUrl: "templates/fund-transfer.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.bank-select', {
			url: "/bank-select",
			views: {
				'menuContent': {
					templateUrl: "templates/bank-select.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.fmcTofmc', {
			url: "/fmcTofmc",
			views: {
				'menuContent': {
					templateUrl: "templates/fmcTofmc.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.fmcToAccount', {
			url: "/fmcToAccount",
			views:{
				'menuContent': {
					templateUrl: "templates/fmcToAccount.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.fmcToPhoneNumber', {
			url: "/fmcToPhoneNumber",
			views:{
				'menuContent': {
					templateUrl: "templates/fmcToPhoneNumber.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.bank-selected', {
			url: "/bank-selected/:bank",
			views:{
				'menuContent': {
					templateUrl: "templates/bank-selected.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.bills', {
			url: "/bills",
			views: {
				'menuContent' : {
					templateUrl: "templates/bills.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.payment-choice', {
			url: "/payment-choice/:name/:vendor/:itemId/:amount",
			views: {
				'menuContent' : {
					templateUrl: "templates/payment-choice.html",
					controller: "paymentChoiceCtrl"
				}
			}
		})
		.state('app.bill-selected', {
			url: "/bill/:bill",
			views: {
				'menuContent' : {
					templateUrl: "templates/bouquet.html",
					controller: "bouquetCtrl"
				}
			}
		})
		.state('app.payment-bill', {
			url: "/payment-bill/:name/:vendor/:itemId/:amount",
			views: {
				'menuContent' : {
					templateUrl: "templates/payment-bill.html",
					controller: "paymentBillCtrl"
				}
			}
		})
		.state('app.airtimes-select', {
			url: "/airtimes",
			views: {
				'menuContent' : {
					templateUrl: "templates/airtime-select.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.airtime-selected', {
			url: "/airtime/:airtime",
			views: {
				'menuContent' : {
					templateUrl: "templates/airtime-selected.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.services',{
			url: "/services",
			views: {
				'menuContent' :{
					templateUrl : "templates/services.html",
					controller: "mainCtrl"
				}
			}
		})
		.state('app.account-funding',{
			url: "/account-funding",
			views: {
				'menuContent' :{
					templateUrl : "templates/account_funding.html",
					controller: "mainCtrl"
				}
			}
		});
		
		$urlRouterProvider.otherwise('/menu/splash');
});