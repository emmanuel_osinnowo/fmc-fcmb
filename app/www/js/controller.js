angular.module('main.controller', [])
	.run(function($ionicPlatform, $cordovaDevice){
		//MY TODO CODES HERE...
	})
	.controller('mainCtrl', function($scope, MainService,$state, $rootScope,$ionicPopup, $cordovaDevice, $stateParams, $ionicPlatform, $timeout,$location, $ionicLoading, $ionicSlideBoxDelegate, $ionicModal, $ionicSideMenuDelegate){
		
		$ionicPlatform.ready(function(){
			//MY TODO CODES HERE...
		});


		$scope.faqsLoading = true;
		$scope.bills_payment_loader = true;
		$scope.product_services_loader = true;
		$scope.product_services_choose = true;
		$scope.airtimes_loader = true;

		$scope.sPhoneNumber = $stateParams.m_phone_number;
		MainService.setPermaPhoneNumber($stateParams.m_phone_number);
		$scope.phone_number_inserted ;
		var phonen = $scope.phone_number_inserted;
		var d = new Date().toLocaleDateString();

		$scope.SideMenu = {
			enable : true,
		}
		$scope.fmcAirtimeRechargeApiDatas = {
			RechargeType : 'PIN',
			PhoneNumber : null,
			RecipientPhoneNumber: null,
			MobileNetworkId: null,
			Amount: null,
			TransDate : d,
			SecretCode: null
		}
		$scope.fundTransfer = {
			"Para1" : null,
			"Para2" : null,
			"Para3" : null,
			"Para4" : null,
			"FundType" : null
		}
		$scope.loginDetails = {
			'PhoneNumber' : '',
			'PIN' : '',
			'TheDeviceuuId' : null
		}

		$scope.fmcRegistration = {
			'PhoneNumber': '',
			'FirstName' : '',
			'LastName' : '',
			'StreetAddress' : '',
			'CityOfResidence' : '',
			'TheStateId' : null,
			'EmailAddress' : '',
			'DateOfBirth' : '',
			'Occupation' : '',
			'QuestionId' : null,
			'SecretAnswer' : null,
			'DeviceuuId' : null,
		};

		$scope.AuthenRegistration = {
			"Para1" :  null,
			"Para2" : null,
			"AuthType" : null,
			"DeviceuuId" : null
		};

		$scope.FmcFunding = {
			PhoneNumber : null,
			RechargeCode : null,
			SecretCode : null
		};

		$scope.questionsAll = {};
		$scope.statesAll = {};
		$scope.product_services = {};
		$scope.SideMenu.enable = MainService.loginInYesNo();


		$scope.getFmcAirtimeRecharge = function(){
			MainService.fmcAirtimeRecharge().then(function(res){
				alert(JSON.stringify(res));
			})
		}
		$scope.showLoading = function(msg) {
		    $ionicLoading.show({
		      template: '<i class="ion-loading-c"></i>',
		      animation: 'fade-in',
		      showBackdrop: true,
		      showDelay: 5
		    });
		  };
		$scope.hideLoading = function(){
		    $ionicLoading.hide();
		  };

		 $scope.CustAlert = function(template, title) {
		   var alertPopup = $ionicPopup.alert({
		     title: title,
		     template: "<h5>" + template + "</h5>"
		   });
		   alertPopup.then(function(res) {
		    
		   });
		 };

		MainService.returnAllProductServices().then(function(res){
			if(res != null){
				$scope.product_services_loader = false;
			}
			$scope.product_services = res;
			$scope.product_services_back = MainService.returnInstanceProductServices()[$stateParams.ps];
		});

		MainService.getStates().then(function(state){
				$scope.statesAll = state;
			}).then(function(error){
				console.log(error);
		});

		MainService.getQuestions().then(function(res){
			$scope.questionsAll = res;
			console.log(JSON.stringify($scope.questionsAll));
		});

		$scope.UrlNavigator = function(args){
			//$scope.CustAlert(args);
			$location.path(args);
		}
		$scope.funding = function(){
			if($scope.FmcFunding.RechargeCode == null || $scope.FmcFunding.SecretCode == null || $scope.FmcFunding.RechargeCode == "" || $scope.FmcFunding.SecretCode == ""){
				alert('Please all fields are required');
				return;
			}
			$scope.showLoading('Please Wait');
			MainService.setOFmcFunding($scope.FmcFunding);
			MainService.fmcFunding().then(function(res){
				$scope.hideLoading();
				$scope.CustAlert(res.MessageDetail, 'Message');
			}).then(function(err){
				console.log(err);
			});
			$scope.hideLoading();
		} 

		$scope.OFundTransfer = function(args){
			$scope.getPhoneNumber();
			switch(args){
				case 1:
					$scope.fundTransfer.FundType = 1;
				break;

				case 2:
					$scope.fundTransfer.FundType = 2;
				break;

				case 3:
					$scope.fundTransfer.FundType = 3;
				break;

				case 4:
					$scope.fundTransfer.FundType = 4;
				break;
			}
				if($scope.fundTransfer.FundType){
					try{
							$scope.showLoading('Please wait');
							MainService.setOfmcFundTransferData($scope.fundTransfer);
							MainService.fmcFundTransfer().then(function(response){
								$scope.CustAlert(response.MessageDetail);
								//$scope.UrlNavigator(MainService.getMainPathDetails());
								//alert(JSON($rootScope.mainPathDetails));
								$scope.hideLoading();
							}).then(function(error){
								console.log(JSON.stringify(error));
								$scope.hideLoading();
							})
						}catch(e){
							console.log(e);
							$scope.hideLoading();
						}
				}
		}
		
		$scope.OAuthenReg = function(args){
			if($scope.AuthenRegistration.Para1 == "" || $scope.AuthenRegistration.Para2 == "" || $scope.AuthenRegistration.Para1 == null || $scope.AuthenRegistration.Para2 == null){
					$scope.CustAlert("All fields are required, please try again", "Fields");
				return;
			}
				switch(args){
					case 1:
						$scope.AuthenRegistration.AuthType = 1;
					break;

					case 2:
						$scope.AuthenRegistration.AuthType = 2;
					break;

					case 3:
						$scope.AuthenRegistration.AuthType = 3;
					break;
				}
				if($scope.AuthenRegistration.AuthType){
					MainService.setFmcAuthenRegistration($scope.AuthenRegistration);
					MainService.fmcAuthenRegistration().then(function(res){
						if(res.MessageStatus){
							$scope.CustAlert(res.MessageDetail);
							$scope.AuthenRegistration.Para1 = null;
							$scope.AuthenRegistration.Para2 =  null;
							$scope.UrlNavigator('/menu/splash');
							$scope.hideLoading();
						}else{
							$scope.CustAlert(res.MessageDetail);
							$scope.hideLoading();
						}
					}).then(function(error,status){
						console.log('Error: ' + status);
						$scope.hideLoading();
					});
					$scope.hideLoading();
				}
		}
	
		$scope.showFmcDetails = function(){
			$scope.showLoading('Please Wait');
			MainService.setFmcRegistration($scope.fmcRegistration);
			MainService.newFMC().then(function(response){
				$scope.CustAlert(response.MessageDetail);
				$scope.hideLoading();
			}).then(function(error){
				console.log(error);
				$scope.hideLoading();
			});
			$scope.hideLoading();
		}

		$scope.bankselected = $stateParams.bank;
		$scope.billselected = $stateParams.bill;
		$scope.airtimeselected = $stateParams.airtime;

		MainService.fmcFaqs().then(function(res){
			if(res != null){
				$scope.faqsLoading = false;
			}
			$scope.faqs = res;
			MainService.setFmcFaqs(res);
		});

		MainService.fmcBanks().then(function(res){
			$scope.banks = res;
			MainService.setFmcBanks(res);
		});
		
		$scope.bills;
		MainService.fmcMerchants().then(function(success){
			if(success != null){
				$scope.bills_payment_loader = false;
			}
			$scope.bills = success;
		});
		MainService.fmcAirtimeRecharge().then(function(res){
			if(res != null){
				$scope.airtimes_loader = false;
			}
			$scope.airtimes = res;
		});
		$scope.amountAirtime = MainService.returnAirtimeAmounts();
		$scope.product_services_returned = MainService.returnInstanceProductServices();

		$scope.visibility = 'hidden';
		$scope.lockType = "ion-ios-locked-outline";
		$scope.readyToLogin = false;
		$scope.camera = 'camera';


		 $ionicModal.fromTemplateUrl('my-modal.html', {
		    scope: $scope,
		    animation: 'slide-in-up'
		  }).then(function(modal) {
		    $scope.modal = modal;
		  });
		  $scope.openModal = function() {
		    $scope.modal.show();
		  };
		  $scope.closeModal = function() {
		    $scope.modal.hide();
		  };
		  //Cleanup the modal when we're done with it!
		  $scope.$on('$destroy', function() {
		    $scope.modal.remove();
		  });
		  // Execute action on hide modal
		  $scope.$on('modal.hidden', function() {
		    // Execute action
		  });
		  // Execute action on remove modal
		  $scope.$on('modal.removed', function() {
		    // Execute action
		  });

		$scope.openModalView = function(argument) {
			
			$scope.debug = MainService.returnByIds(argument);
			$scope.question = $scope.debug[0].FaqQuestion;
			$scope.answer = $scope.debug[0].FaqAnswer;
			$scope.openModal();
		}

		$scope.pushNotification = function(){
			try{
				var pushNotification = window.plugins.pushNotification;
					pushNotification.register($scope.successHandler, $scope.errorHandler,{"senderID":"214638647172","ecb":"onNotificationGCM"});
			}catch(e){
				console.log(e);
			}
		}
		
		$scope.successHandler = function(result) {
			console.log('Callback Success! Result = ' + result);
		}
		$scope.errorHandler = function(error) {
			console.log(error);
		}
		
		onNotificationGCM = function(e) {
			console.log('inside GCM');
			switch( e.event )
			{
				case 'registered':
					if ( e.regid.length > 0 )
					{
						console.log("Regid " + e.regid);
						console.log('registration id = '+ e.regid);
						$scope.notificationHandler(e.regid);

					}
				break;
	 
				case 'message':
				   console.log('message = '+e.message+' msgcnt = '+e.msgcnt);
				break;
	 
				case 'error':
				  console.log('GCM error = '+e.msg);
				break;
	 
				default:
				  console.log('An unknown GCM event has occurred');
				  break;
			}
		}

		$scope.notificationHandler = function(key){
			MainService.registerKey(key).then(function(response){
				console.log('success');
			})
		}

		$scope.signIn = function(){
			MainService.setLoginYesNo(true);
			$scope.visibility = 'visible';
			$scope.lockType	= "ion-ios-unlocked-outline";
				if(!$scope.readyToLogin){
					$scope.readyToLogin = true;
					$scope.lockType	= "ion-ios-unlocked-outline";
				}else{
					console.log("Scope is ready to Login");
					if($scope.loginDetails.PhoneNumber == "" || $scope.loginDetails.PIN == ""){
						$scope.CustAlert('Please Enter Phone Number and Pin', 'Error');
						return;
					}else{
						$scope.showLoading("Login");
						MainService.setFmcLoginData($scope.loginDetails);
						MainService.fmcLogin().then(function(res){
							//$scope.CustAlert(JSON.stringify(res));
							$scope.hideLoading();
							if(res.MessageStatus){
								$scope.hideLoading();
								$scope.SideMenu.enable = true;
								$scope.phone_number_inserted = res.PhoneNumber;
								$scope.storePhoneNumber();
								if($scope.phone_number_inserted){
									MainService.setEnabledSideMenu(true);
									//MainService.setMainPathDetails('/menu/main/' + res.FMCbalance + '/' + res.CustomerId + '/' + res.FirstName + '/' + res.LastName + '/' + res.PhoneNumber + '/' + res.EmailAddress);
									$rootScope.mainPathDetails = {
										balance : res.FMCbalance,
										customer_id : res.CustomerId,
										first_name : res.FirstName,
										last_name :  res.LastName ,
										phone_number :  res.PhoneNumber,
										email : res.EmailAddress 
									} 

									//alert(JSON.stringify($rootScope.mainPathDetails));
									$scope.CustAlert(res.MessageDetail, 'Login');
									$location.path('/menu/main/' + res.FMCbalance + '/' + res.CustomerId + '/' + res.FirstName + '/' + res.LastName + '/' + res.PhoneNumber + '/' + res.EmailAddress);
								}else{
									$scope.hideLoading();
									$scope.CustAlert(res.MessageDetail, 'Error');
								}

							}else{
								$scope.hideLoading();
								$scope.CustAlert(res.MessageDetail, 'Error');
							}
						});

						return;
						MainService.login($scope.loginDetails.phone_number, $scope.loginDetails.pin).then(function(res){
							if(res.RESPONSE_TYPE){
								MainService.setCurrentDetail(res.DETAIL.last_name, res.DETAIL.other_names, res.DETAIL.account_number);
								$scope.hideLoading();
								$location.path('/menu/main');
							}else{
								$scope.hideLoading();
								alert(res.RESPONSE);
							}
							
						});
		
					}
				}
				
				
		}
		$scope.takePicture = function(){
			$scope.camera = '';
			try{
					navigator.camera.getPicture(function(path){
						$scope.imaging = path ;
						$scope.insertPicture(path);
					}, function(error){
						console.log(error.message);
						$scope.camera = 'camera';
					}, { quality: 50,
					 destinationType: Camera.DestinationType.FILE_URI,
					 encodingType: Camera.EncodingType.JPEG,sourceType:Camera.PictureSourceType.CAMERA});
			}catch(e){
				console.log(e);
				$scope.camera = 'camera';
			}
		}
		
		$scope.getPhoneNumber = function(){
			var db = window.openDatabase("FCMB", "1.0", "FCMB LocalDB", 200000);
				db.transaction($scope.retrievePhoneNumber, $scope.errorRetrievePhoneNumber, $scope.successRetrievePhoneNumber);
		}

		$scope.retrievePhoneNumber = function(tx){
				tx.executeSql("SELECT * FROM `PhoneNumber`", [], function(tx, results){
					$scope.fundTransfer.Para1 = results.rows.item(0).phone_number;
					$scope.phone_number_inserted = results.rows.item(0).phone_number;
					$scope.FmcFunding.PhoneNumber = results.rows.item(0).phone_number;
					$scope.fmcAirtimeRechargeApiDatas.PhoneNumber = results.rows.item(0).phone_number;
					//alert(JSON.stringify($scope.FmcFunding));
				}, function(error){
					console.log('Error - inside retrievePhoneNumber');
				});
		}

		$scope.errorRetrievePhoneNumber = function(error){
			console.log("Error: " + JSON.stringify(error));
		}

		$scope.successRetrievePhoneNumber = function(success){
			console.log('Success: ' + JSON.stringify(success));
		}	

		$scope.storePhoneNumber = function(){
			var db = window.openDatabase("FCMB", "1.0", "FCMB LocalDB", 200000);
				db.transaction($scope.insertPhoneNumber, $scope.errorPhoneNumber, $scope.successPhoneNumber);
		}

		$scope.errorPhoneNumber = function(error){
			console.log('Error: ' + error);
		}

		$scope.successPhoneNumber = function(success){
			console.log('Success: ' + success);
		}

		$scope.insertPhoneNumber = function(tx){
			tx.executeSql("DELETE FROM `PhoneNumber`");
			tx.executeSql("INSERT INTO `PhoneNumber` VALUES(" + $scope.phone_number_inserted + ")");
		}

		$scope.createDatabase = function(){
			var db = window.openDatabase("FCMB", "1.0", "FCMB LocalDB", 200000);
				db.transaction($scope.create, $scope.errorCreateDatabase, $scope.successCreateDatabase);
			
		}
		
		$scope.create = function(tx){
			tx.executeSql('DROP TABLE IF EXISTS `customer_picture`');
			tx.executeSql('CREATE TABLE IF NOT EXISTS `customer_picture`(`picture_name` TEXT)');
			tx.executeSql('CREATE TABLE IF NOT EXISTS `PhoneNumber` (`phone_number` TEXT)')
		}
		
		$scope.errorCreateDatabase = function(error){
			console.log(error.message);
		}
		
		$scope.successCreateDatabase = function(success){
			console.log(success);
		}
		
		$scope.retrievePhoto = function(){
			var db = window.openDatabase("FCMB", "1.0", "FCMB LocalDB", 200000);
				db.transaction(function(tx){
					tx.executeSql("SELECT * FROM `customer_picture`", [], function(tx, results){
						if(!results.rows.length == 0){
							$scope.imaging = results.rows.item(0).picture_name;
							console.log($scope.imaging);
						}
					}, function(error){
						console.log(error.message);
					});
				},function(error){
					console.log(error.message);
				}, function(success){
					console.log(success);
				})
		}
		
		$scope.insertPicture = function(imageURL){
			var db = window.openDatabase("FCMB", "1.0", "FCMB LocalDB", 200000);
				 db.transaction(function(tx){
					tx.executeSql("DELETE FROM `customer_picture`");
					tx.executeSql("INSERT INTO `customer_picture` VALUES ('" + imageURL +"')");
				 }, function(error){
					console.log(error.message);
				 }, function(sucess){
					console.log(success);
				 });
		}

		$scope.$watch('imaging', function(){
			$scope.retrievePhoto();
		});
		
		$timeout(function(){
			$scope.pushNotification();
			$scope.retrievePhoto();
			$scope.loginDetails.TheDeviceuuId = $cordovaDevice.getDevice().uuid;
			$scope.AuthenRegistration.DeviceuuId = $cordovaDevice.getDevice().uuid;
			$scope.fmcRegistration.DeviceuuId = $cordovaDevice.getDevice().uuid;
		}, 3000);
		$scope.createDatabase();
		$scope.retrievePhoto();
		$scope.getPhoneNumber();
		var getTelephone = function(){
			var telephoneNumber = cordova.require("cordova/plugin/telephonenumber");
				telephoneNumber.get(function(result) {
				        console.log("result = " + result);
				        $scope.loginDetails.phone_number = result;
				    }, function() {
				        console.log("error");
				    });
		}
	
	})
	.controller('mainCtrl2', function($scope, MainService){

		
	})
	.controller('registerCtrl', function($scope, $location,$location, MainService){
		$scope.regDetails = {
			'bank_card' : '',
			'account_number' : '',
			'secret_questions' : '',
			'answer' : ''
		};

		MainService.returnAllQuestions().then(function(res){
			$scope.allQuestions = res;
		});

		$scope.debug = function(){
			alert('registerCtrl.debug()');
		}

		$scope.registerNow = function(type){
			var xb = MainService.returnFilteredQuestion($scope.regDetails.secret_questions);

			var device_uuid = device.uuid;
			switch(type){
				case 'card':
						if($scope.regDetails.bank_card == "" || $scope.regDetails.account_number == ""){
								alert('Please Enter Phone Number and Pin')('All fields are must be provided');
								return;
						}
					MainService.registrationViaCard($scope.regDetails.bank_card, $scope.regDetails.account_number,xb[0].question_id, $scope.regDetails.answer, device_uuid).then(function(response, status){
						if(response.RESPONSE_TYPE){
								$scope.regDetails.bank_card = '';
								$scope.regDetails.account_number = '';
								$scope.regDetails.secret_questions = '';
								$scope.regDetails.answer = '';
								alert(response.RESPONSE);
						}else{
								$scope.regDetails.bank_card = '';
								$scope.regDetails.account_number ='';
								$scope.regDetails.secret_questions = '';
								$scope.regDetails.answer = '';
								alert(response.RESPONSE);
								return;
							switch(device.platform){
								case "Android":
									alert(response.RESPONSE);
								break;

								default:
									alert(response.RESPONSE);
								break;
							}
								
						}
					}).then(function(data, status){
						console.log(data);
					});
				break;

				case 'internet_banking':
					alert('coming soon');
					return;
					MainService.registrationViaInternetBanking($scope.regDetails.internet_banking, $scope.regDetails.account_number).then(function(res){
						if(res.RESPONSE_TYPE){
							alert('Yeah');
						}else{
							alert('Nah');
						}
					})
				break;

				case '':
				break;

			}
		}
		$scope.navigateUrl = function(_params){
				switch(_params){
					case 'agreed':
						$location.path('/menu/register-select');
					break;

					case 'not-agreed':
						$location.path('/menu/splash');
					break;
				}	
		}


	})

	.controller('mapCtrl', function($scope, FcmbLocations, $timeout){
			var cities = FcmbLocations.returnAll();
			
			$scope.map = {
				display : true
			}

			FcmbLocations.returnAllBranches().then(function(res, param2, param3){
				cities = res;
				if(typeof(res) == 'string' || res ==  null){
					$scope.map.display = true;
				}else{
					$scope.map.display = false;
				}

				$scope.getCurrentLocationWithGoogleMap = function(){
				 navigator.geolocation.getCurrentPosition(showPosition);
				}

			var showPosition = function(position){

			}
		    var mapOptions = {
		        zoom: 35,
		        center: new google.maps.LatLng(cities[0].BranchLatitude,cities[0].BranchLongitude),
		        mapTypeId: google.maps.MapTypeId.TERRAIN
		    }

		    $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

		    $scope.markers = [];
		    
		    var infoWindow = new google.maps.InfoWindow();
		    
		    var createMarker = function (info){
		        
		        var marker = new google.maps.Marker({
		            map: $scope.map,
		            position: new google.maps.LatLng(info.BranchLatitude, info.BranchLongitude),
		            title: info.BranchName
		        });
		        marker.content = '<div class="infoWindowContent">' + info.BranchLocation + '</div>';
		        
		        google.maps.event.addListener(marker, 'click', function(){
		            infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
		            infoWindow.open($scope.map, marker);
		        });
		        
		        $scope.markers.push(marker);
		        
		    }  
		    for (i = 0; i < cities.length; i++){
		        createMarker(cities[i]);
		    }

		    $scope.openInfoWindow = function(e, selectedMarker){
		        e.preventDefault();
		        google.maps.event.trigger(selectedMarker, 'click');
		    }
			}).then(function(err){
				console.log(JSON.stringify(err));
			});

	})
	.controller('bouquetCtrl', function($scope,$location, $stateParams, MainService){

		$scope.bouquets_loader = true;

		$scope.bouquets = [] ;
		$scope.vendor = $stateParams.bill;

		$scope.UrlNavigator = function(args){

			$location.path(args);
		}

		MainService.fmcBouquet($stateParams.bill).then(function(success){
			if(success != null){
				$scope.bouquets_loader = false;
			}
			$scope.bouquets = success;
		}).then(function(err){
			//TODO Codes here
		});
	})
	.controller('paymentChoiceCtrl', function($scope,$stateParams,$location,MainService){
		$scope.name = $stateParams.name;
		$scope.vendor = $stateParams.vendor;
		$scope.itemId = $stateParams.itemId;
		$scope.amount = $stateParams.amount;

		$scope.UrlNavigator = function(args){
			$location.path(args);
		}

	})
	.controller('paymentBillCtrl', function($scope, $stateParams,$ionicLoading,$ionicPopup, MainService){
		$scope.name = $stateParams.name;
		$scope.vendor = $stateParams.vendor;
		$scope.itemId = $stateParams.itemId;
		$scope.amount = $stateParams.amount;

		$scope.paymentDetails = {
		  "PhoneNumber": null,
		  "Vendor": $scope.vendor,
		  "CustomerIdentication": 1,
		  "Amount": $scope.amount,
		  "SecretCode": null,
		  "BillableItem": $scope.itemId
		}

		$scope.UrlNavigator = function(args){
			//$scope.CustAlert(args);
			$location.path(args);
		}

		 $scope.CustAlert = function(template, title) {
		   var alertPopup = $ionicPopup.alert({
		     title: title,
		     template: "<h5>" + template + "</h5>"
		   });
		   alertPopup.then(function(res) {
		     console.log('Thank you for not eating my delicious ice cream cone');
		   });
		 }

		 $scope.showLoading = function(msg) {
		    $ionicLoading.show({
		      template: msg
		    });
		  };
		
		$scope.hideLoading = function(){
		    $ionicLoading.hide();
		  };

		$scope.pay = function(){
			$scope.showLoading('Please Wait');
			MainService.getPhoneNumber().then(function(res){
				$scope.paymentDetails.PhoneNumber = res;
				MainService.fmcPayPaymentBill($scope.paymentDetails).then(function(res){
					$scope.hideLoading();
					$scope.CustAlert(res.MessageDetail);
				});
				
			});
		}

	})
	.controller('sideMenuCtrl', function($scope, $location,$timeout,$interval,$stateParams, $ionicSlideBoxDelegate, $ionicModal, $ionicSideMenuDelegate, MainService){
		
		$scope.firstSlide = "#000000";
		$scope.secondSlide = "#FFFFFF";
		$scope.thirdSlide = "#FFFFFF";
		$scope.sideMenuBarEnabled = MainService.getEnabledSideMenu();
		$scope.m_accountDetails = {
			balance : $stateParams.m_balance,
			customerId : $stateParams.m_id,
			firstName : $stateParams.m_first_name,
			lastName : $stateParams.m_last_name,
			phone_number : $stateParams.m_phone_number,
			email: $stateParams.m_email
		}

		$scope.slideTo = function(index){
			 $ionicSlideBoxDelegate.slide(index);
			 $scope.swipeSlide(index);
		}

		$scope.toggleLeft = function() {
			$ionicSideMenuDelegate.toggleLeft();
		}
		
		$scope.swipeSlide = function(index){
			switch(index){
				case 0:
					$scope.firstSlide = "#000000";
					$scope.secondSlide = "#FFFFFF";
					$scope.thirdSlide = "#FFFFFF";
				break;
				
				case 1:
					$scope.firstSlide = "#FFFFFF";
					$scope.secondSlide = "#000000";
					$scope.thirdSlide = "#FFFFFF";
				break;
				
				case 2:
					$scope.firstSlide = "#FFFFFF";
					$scope.secondSlide = "#FFFFFF";
					$scope.thirdSlide = "#000000";
				break;
			}
		}

		MainService.returnEnabledSideMenu().then(function(res){
			//$scope.sideMenuBarEnabled = false;
		});

		$scope.launchNewPage = function(page){
			switch(page){
				case 'transfers':
				
					$scope.accounts_background = "";
					$scope.accounts_border_left = "";
					
					$scope.transfers_border_left = "3px solid #5500FF";
					$scope.transfers_background = "#000000";
					
					$scope.airtime_payment_background = "";
					$scope.airtime_payment_border_left = "";
					
					$scope.bills_payments_background = "";
					$scope.bills_payments_border_left = "";
					
					$scope.services_background = "";
					$scope.services_border_left = "";
					
					$scope.product_services_background = "";
					$scope.product_services_border_left = "";
					
					$scope.service_and_help_background = "";
					$scope.service_and_help_border_left = "";

					$scope.account_funding_background = "";
					$scope.account_funding_border_left =  "";

					$location.path('/menu/fund-transfer/' + $scope.sPhoneNumber);
				break;
				
				case 'accounts':
					$scope.accounts_background = "#000000";
					$scope.accounts_border_left = "3px solid #5500FF";
					
					$scope.transfers_background = "";
					$scope.transfers_border_left = "";
					
					$scope.airtime_payment_background = "";
					$scope.airtime_payment_border_left = "";
					
					$scope.bills_payments_background = "";
					$scope.bills_payments_border_left = "";
					
					$scope.services_background = "";
					$scope.services_border_left = "";
					
					$scope.product_services_background = "";
					$scope.product_services_border_left = "";
					
					$scope.service_and_help_background = "";
					$scope.service_and_help_border_left = "";

					$scope.account_funding_background = "";
					$scope.account_funding_border_left =  "";
					
				break;
				
				case 'airtime_payment':
					$scope.accounts_background = "";
					$scope.accounts_border_left = "";
					
					$scope.transfers_background = "";
					$scope.transfers_border_left = "";
					
					$scope.airtime_payment_background = "#000000";
					$scope.airtime_payment_border_left = "3px solid #5500FF";
					
					$scope.bills_payments_background = "";
					$scope.bills_payments_border_left = "";
					
					$scope.services_background = "";
					$scope.services_border_left = "";
					
					$scope.product_services_background = "";
					$scope.product_services_border_left = "";
					
					$scope.service_and_help_background = "";
					$scope.service_and_help_border_left = "";

					$scope.account_funding_background = "";
					$scope.account_funding_border_left =  "";

					$location.path('/menu/airtimes');
				break;
				
				case 'bills_payments':
					$scope.accounts_background = "";
					$scope.accounts_border_left = "";
					
					$scope.transfers_background = "";
					$scope.transfers_border_left = "";
					
					$scope.airtime_payment_background = "";
					$scope.airtime_payment_border_left = "";
					
					$scope.bills_payments_background = "#000000";
					$scope.bills_payments_border_left = "3px solid #5500FF";
					
					$scope.services_background = "";
					$scope.services_border_left = "";
					
					$scope.product_services_background = "";
					$scope.product_services_border_left = "";
					
					$scope.service_and_help_background = "";
					$scope.service_and_help_border_left = "";

					$scope.account_funding_background = "";
					$scope.account_funding_border_left =  "";

					$location.path('/menu/bills');
				break;
				
				case 'services':
					$scope.accounts_background = "";
					$scope.accounts_border_left = "";
					
					$scope.transfers_background = "";
					$scope.transfers_border_left = "";
					
					$scope.airtime_payment_background = "";
					$scope.airtime_payment_border_left = "";
					
					$scope.bills_payments_background = "";
					$scope.bills_payments_border_left = "";
					
					$scope.services_background = "#000000";
					$scope.services_border_left = "3px solid #5500FF";
					
					$scope.product_services_background = "";
					$scope.product_services_border_left = "";
					
					$scope.service_and_help_background = "";
					$scope.service_and_help_border_left = "";

					$scope.account_funding_background = "";
					$scope.account_funding_border_left =  "";

					$location.path("/menu/services");
				break;
				
				case 'product_services':
					$scope.accounts_background = "";
					$scope.accounts_border_left = "";
					
					$scope.transfers_background = "";
					$scope.transfers_border_left = "";
					
					$scope.airtime_payment_background = "";
					$scope.airtime_payment_border_left = "";
					
					$scope.bills_payments_background = "";
					$scope.bills_payments_border_left = "";
					
					$scope.services_background = "";
					$scope.services_border_left = "";
					
					$scope.product_services_background = "#000000";
					$scope.product_services_border_left = "3px solid #5500FF";
					
					$scope.service_and_help_background = "";
					$scope.service_and_help_border_left = "";

					$scope.account_funding_background = "";
					$scope.account_funding_border_left =  "";

					$location.path("/menu/product-services");
				break;
				
				case 'service_and_help':
					$scope.accounts_background = "";
					$scope.accounts_border_left = "";
					
					$scope.transfers_background = "";
					$scope.transfers_border_left = "";
					
					$scope.airtime_payment_background = "";
					$scope.airtime_payment_border_left = "";
					
					$scope.bills_payments_background = "";
					$scope.bills_payments_border_left = "";
					
					$scope.services_background = "";
					$scope.services_border_left = "";
					
					$scope.product_services_background = "";
					$scope.product_services_border_left = "";
					
					$scope.service_and_help_background = "#000000";
					$scope.service_and_help_border_left = "3px solid #5500FF";

					$scope.account_funding_background = "";
					$scope.account_funding_border_left =  "";
				break;

				case 'account_funding':
					$scope.accounts_background = "";
					$scope.accounts_border_left = "";
					
					$scope.transfers_background = "";
					$scope.transfers_border_left = "";
					
					$scope.airtime_payment_background = "";
					$scope.airtime_payment_border_left = "";
					
					$scope.bills_payments_background = "";
					$scope.bills_payments_border_left = "";
					
					$scope.services_background = "";
					$scope.services_border_left = "";
					
					$scope.product_services_background = "";
					$scope.product_services_border_left = "";
					
					$scope.service_and_help_background = "";
					$scope.service_and_help_border_left = "";

					$scope.account_funding_background = "#000000";
					$scope.account_funding_border_left =  "3px solid #5500FF";

					$location.path("/menu/account-funding");
				break;
				
			}
		}

		$timeout(function(){
			$scope.launchNewPage('accounts');
		}, 300);
		$interval(function(){
			$scope.sideMenuBarEnabled = MainService.getEnabledSideMenu();
		}, 500);
	});