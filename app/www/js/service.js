angular.module('main.services', [])

	.factory('MainService', function($q, $http){
		var questions;
		var loginDetails;
		var fmcRegistrationDetails;
		var fmcLoginData;
		var allProductServices;
		var OAuthenData;
		var permaPhoneNumnber;
		var OfundTransferData;
		var OFmcFundingData;
		var loginYN = true;
		var faqs;
		var banks;
		var enableSideMenu = false;
		var bills ;
		var airtime;
		var fmcAirtimeRechargeApiData;
		var mainPathDetails;

		var airtime_amount = [
			{"amount" : 100},
			{"amount" : 200},
			{"amount" : 400},
			{"amount" : 750},
			{"amount" : 1500},
			{"amount" : 2500}
		];

		var detailofCurrentUser = {
			last_name : '',
			other_name : '',
			account_number: ''
		}
		
		return{
			getMainPathDetails : function(){
				return mainPathDetails;
			},
			setMainPathDetails: function(args){
				mainPathDetails = args;
			},
			getFmcAirtimeRechargeApiData: function(){
				return fmcAirtimeRechargeApiData;
			},
			setFmcAirtimeRechargeApiData: function(args){
				fmcAirtimeRechargeApiData = args;
			},
			fmcAirtimeRechargeApi: function(){
				var deffered = $q.defer();
				var promise = deffered.promise;
					$http({
						method: "POST",
						headers: {"Content-Type" : "application/json;charset=utf-8"},
						url: "http://olusegun.org.ng/api/AirtimeRechargeApi",
						data: fmcAirtimeRechargeApiData
					}).success(function(success){
						deffered.resolve(success);
					}).error(function(err){
						deffered.resolve(err);
					});
					return promise;
			},
			fmcAirtimeRecharge: function(){
				var deffered = $q.defer();
				var promise = deffered.promise;
					$http({
						method: "GET",
						headers: {'Content-Type': 'application/json;charset=utf-8'},
						url: "http://olusegun.org.ng/api/AirtimeRechargeApi/GetMobileNetworks"
					}).success(function(success){
						airtime = success;
						deffered.resolve(success);
					}).error(function(error){
						deffered.resolve;
					});
					return promise;
			},
			getEnabledSideMenu: function(){
				return enableSideMenu;
			},
			setEnabledSideMenu: function(args){
				enableSideMenu = args;
			},
			returnEnabledSideMenu : function(){
				var deffered = $q.defer();
				var promise = deffered.promise;
				deffered.resolve(enableSideMenu);
				return promise;
			},
			fmcMobileNetworks : function(){
				var deffered = $q.defer();
				var promise = deffered.promise;

				$http({
					method: "POST",
					data: params,
					headers : {'Content-Type' : 'application/json;charset=utf-8'},
					url: "http://olusegun.org.ng/api/api/AirtimeRechargeApi/GetMobileNetworks"
				}).success(function(success){
					deffered.resolve(success);
				}).error(function(err){
					deffered.resolve(err);
				});
				return promise;
			},
			fmcPayPaymentBill: function(params){
				var deffered = $q.defer();
				$http({
						method: "POST",
						data: params,
						headers: {'Content-Type' : 'application/json;charset=uft-8'},
						url: "http://olusegun.org.ng/api/BillPaymentApi/PostBillPaymentAccount"
					}).success(function(response){
						deffered.resolve(response);
					}).error(function(error){
						deffered.resolve(error);
					})
				return deffered.promise;
			},
			getPhoneNumber: function(){
				var deffered = $q.defer();
				var db = window.openDatabase("FCMB", "1.0", "FCMB LocalDB", 200000);
					db.transaction(function(tx){
						tx.executeSql("SELECT * FROM `PhoneNumber`", [], function(tx, results){
							deffered.resolve(results.rows.item(0).phone_number);
						}, function(error){
							console.log('Error - inside retrievePhoneNumber');
						});
					}, function(error){
						console.log('Error' + error);
					}, function(success){
						console.log('Success' + success);
					});
				return deffered.promise;
			},
			fmcBouquet: function(args){
				var deffered = $q.defer();
			   		$http({
						method: "GET",
						params : {
							id : args
						},
						headers:  {'Content-Type' : 'application/json;charset=uft-8'},
						url: "http://olusegun.org.ng/api/BillPaymentApi/GetBillableItem"
					}).success(function(success){
						//alert(JSON.stringify(success));
						deffered.resolve(success);
					}).error(function(err){
						deffered.resolve(err);
					})
				return deffered.promise;
			},
			setFmcMerchants: function(params){
				bills = params;
			},
			getFmcMerchants : function(params){
				return bills;
			},
			fmcMerchants: function(){
				var deffered = $q.defer();
			   		$http({
						method: "GET",
						headers:  {'Content-Type' : 'application/json;charset=uft-8'},
						url: "http://olusegun.org.ng/api/MerchantApi"
					}).success(function(success){
						deffered.resolve(success);
					}).error(function(err){
						deffered.resolve(err);
					})
				return deffered.promise;
			},
			setFmcBanks : function(params){
				banks = params;
			},
			fmcBanks: function(){
				var deffered = $q.defer();
				$http({
						method: "GET",
						headers: {'Content-Type' : 'application/json;charset=uft-8'},
						url: "http://olusegun.org.ng/api/BankApi"
					}).success(function(response){
						deffered.resolve(response);
					}).error(function(error){
						deffered.resolve(error);
					})
				return deffered.promise;
			},
			setFmcFaqs : function(params){
				faqs = params;
			},
			fmcFaqs: function(){
				var deffered = $q.defer();
				$http({
						method: "GET",
						headers: {'Content-Type' : 'application/json;charset=uft-8'},
						url: "http://olusegun.org.ng/api/FaqApi"
					}).success(function(response){
						deffered.resolve(response);
					}).error(function(error){
						deffered.resolve(error);
					})
				return deffered.promise;
			},
			getOFmcFunding: function(args){
				return OFmcFundingData;
			},
			setOFmcFunding: function(args){
				OFmcFundingData = args;
			},
			fmcFunding: function(){
				var deffered = $q.defer();
				$http({
						method: "POST",
						data: OFmcFundingData,
						headers: {'Content-Type' : 'application/json;charset=uft-8'},
						url: "http://olusegun.org.ng/api/FundFMCApi"
					}).success(function(response){
						deffered.resolve(response);
					}).error(function(error){
						deffered.resolve(error);
					})
				return deffered.promise;
			},
			setLoginYesNo : function(args){
				loginYN = args;
			},
			loginInYesNo : function(){
				return loginYN;
			},
			setOfmcFundTransferData: function(args){
				OfundTransferData = args;
			},
			getOfmcFundTransferData: function(){
				return OfundTransferData;
			},
			fmcFundTransfer: function(){
				var deffered = $q.defer();
					$http({
						method: 'POST',
						data: OfundTransferData,
						headers: {'Content-Type': 'application/json;charset=uft-8'},
						url: 'http://olusegun.org.ng/api/FundTransferApi'
					}).success(function(res){
						deffered.resolve(res);
					}).error(function(err){
						deffered.resolve(err);
					})
				return deffered.promise;
			},
			getPermaPhoneNumber : function(){
				return permaPhoneNumnber;
			},
			setPermaPhoneNumber : function(param){
				permaPhoneNumnber = param;
			},
			setFmcAuthenRegistration: function(args){
				OAuthenData = args;
			},
			getFmcAuthenRegistration: function(){
				return OAuthenData;
			},
			fmcAuthenRegistration: function(){
				var deffered = $q.defer();
					$http({
						method: 'POST',
						data: OAuthenData,
						headers: {'Content-Type': 'application/json;charset=uft-8'},
						url: 'http://olusegun.org.ng/api/AuthValApi'
					}).success(function(res){
						deffered.resolve(res);
					}).error(function(err,status, headers){
						deffered.resolve(status);
					});
				return deffered.promise;
			},
			returnInstanceProductServices: function(){
				return allProductServices;
			},
			returnAllProductServices : function(){
				var deffered = $q.defer();
				$http({
						method: 'GET',
						url: "http://olusegun.org.ng/api/ProductServiceApi"
					}).success(function(res){
						allProductServices = res;
						deffered.resolve(res);
					}).error(function(err){
						deffered.resolve(err);
					});
				return deffered.promise;
			},
			getFmcLoginData: function(){
				return fmcLoginData;
			},
			setFmcLoginData: function(params){
				fmcLoginData = params;
			},
			fmcLogin: function(){
				var deffered = $q.defer();
					$http({
						method: 'POST',
						data: fmcLoginData,
						headers: {'Content-Type': 'application/json;charset=uft-8'},
						url: 'http://olusegun.org.ng/api/LoginApi'
					}).success(function(res){
						deffered.resolve(res);
					}).error(function(err){
						deffered.resolve(err);
					})
				return deffered.promise;
			},
			getStates: function(){
				var deffered = $q.defer();
					$http({
						method: "GET",
						url: "http://olusegun.org.ng/api/TheStateApi"
					}).success(function(res, status, headers){
						deffered.resolve(res);
					}).error(function(err, status){
						deffered.resolve(err);
					});
				return deffered.promise;

			},
			getQuestions: function(){
				var deffered = $q.defer();
					$http({
						method: "GET",
						url: "http://olusegun.org.ng/api/SecretQuestionApi"
					}).success(function(res, status, headers){
						deffered.resolve(res);
					}).error(function(err){
						deffered.reject(err);
					});
				return deffered.promise;
			},
			newFMC: function(){
				var deffered =  $q.defer();
					$http({
						method: "POST",
						url: "http://olusegun.org.ng/api/CustomerApi",
						data: fmcRegistrationDetails,
						headers: {'Content-Type': 'application/json;charset=uft-8'},
						timeout: 50000,
						}).success(function(res, status, headers){
						deffered.resolve(res);
					})
					.error(function(err, status, headers){
						deffered.resolve(err);
					});
				return deffered.promise;
			},
			setFmcRegistration : function(param){
				console.log(param);
				fmcRegistrationDetails = param;
			},
			getFmcRegistration : function(){
				return fmcRegistrationDetails;
			},
			returnCurrenUserDetails : function(){
				return loginDetails;
			},
			returnFilteredQuestion : function(params){
					var results = questions.filter(function(element){
					return params === element.question_name;
				});

				return results;
			},
			setCurrentDetail : function(last_name, other_names, account_number){
				detailofCurrentUser.last_name = last_name;
				detailofCurrentUser.other_names = other_names;
				detailofCurrentUser.account_number = account_number;
			},
			getCurrentDetail : function(){
				return detailofCurrentUser;
			},
			//http://www.upltest.com/fcmb_push_notification/fcmb_php_files
			returnAllQuestions: function(){
				var deffered =  $q.defer();
					$http.get("http://localhost/fcmb_backend/class.questions.php")
					.success(function(res){
						questions = res;
						deffered.resolve(res);
					})
					.error(function(err){
						deffered.reject(err);
					});
				return deffered.promise;
			},
			returnAirtimeAmounts : function(){
				return airtime_amount;
			},
			returnAllAirtimes: function(){
				return airtime;
			},
			returnAllBills: function(){
				return bills;
			},
			returnAllBanks: function(){
				return banks;
			},
			returnAll:function(){
				return faqs;
			},
			returnByIds: function(param){
				var results = faqs.filter(function(element){
					return parseInt(param) === parseInt(element.FaqId);
				});
				return results;
			},
			login:function(phone, p){
				var deffered = $q.defer();
				$http.get("http://localhost/fcmb_backend/class.login.php", 
						{
							params: {
								__reg_type: 'login',
								phone_number: phone,
								pin: p
							},
							timeout: 30000
						})
					.success(function(dataResponse, status){
						loginDetails = dataResponse;
						deffered.resolve(dataResponse);
					})
					.error(function(data, status, headers, config){
						deffered.reject(data);
					});

					return deffered.promise;
			},
			registrationViaCard:function(card, account, question_id, answer, device_uuid){
				var deffered = $q.defer();
				$http.get("http://localhost/fcmb_backend/class.card.registration.php", 
						{
							params: {
								__reg_type: 'card',
								bank_card: card,
								account_number: account,
								question_id: question_id,
								answer: answer,
								device_uuid: device_uuid,
							},
							timeout: 30000
						})
					.success(function(dataResponse, status){
						deffered.resolve(dataResponse);
					})
					.error(function(data, status, headers, config){
						deffered.reject(data);
					});

					return deffered.promise;
			},
			registerKey: function(key){
				var deffered = $q.defer();
					$http.get("http://www.upltest.com/fcmb_push_notification/index.php",
					{
						params: {
							ops_type: 'insert',
							reg_id : key
						}
					}).success(function(dataResponse, status){
						deffered.resolve(dataResponse);
					}).error(function(data, status, headers, config){
						deffered.reject(data);
					})
				return deffered.promise;
			},
			sendNotification: function(){
				var deffered = $q.defer();
					$http.get("http://www.upltest.com/fcmb_push_notification/index.php",
					{
						params: {
							ops_type: 'send'
						}
					}).success(function(dataResponse, status){
						deffered.resolve(dataResponse);
					}).error(function(data, status, headers, config){
						deffered.reject(data);
					})
				return deffered.promise;
			},
			registrationViaInternetBanking: function(param1, param2){
				var deffered = $q.defer();
					$http.get("http://localhost/fcmb_backend/class.card.registration.php", {
						params: {
								__reg_type: 'internet_banking',
								registration_code: param1,
								account_number: param2
						}
					}).success(function(dataResponse, status){
						deffered.resolve(dataResponse);
					})
					.error(function(data, status, headers, config){
						deffered.reject(data);
					});
				return deffered.promise;
			}
		}
	
	})
	
	.factory('FcmbLocations', function($q, $http){

		var cities = [
		    {
		        city : 'FCMB Agege Motor Road',
		        desc : 'Lagos, Nigeria',
		        lat : 6.5480747,
		        long : 3.3975005
		    },
		    {
		        city : 'FCMB Ogba Road',
		        desc : 'Lagos, Nigeria',
		        lat : 6.5480747,
		        long : 3.3975005
		    },
		    {
		        city : 'First City Monument Bank Allen Avenue',
		        desc : 'Lagos, Nigeria',
		        lat : 6.5480747,
		        long : 3.3975005
		    },
		    {
		        city : 'FCMB',
		        desc : 'Lagos, Nigeria',
		        lat : 34.0500,
		        long : -118.2500
		    },
		    {
		        city : 'FCMB Bank',
		        desc : 'Lagos, Nigeria',
		        lat : 36.0800,
		        long : -115.1522
		    }
		];

		return {
			returnAll: function(){
				return cities;
			},
			returnAllBranches : function(){
				var deffered = $q.defer();
					$http({
						method: 'GET',
						url: "http://olusegun.org.ng/api/BranchFinderApi"
					}).success(function(res,param2, param3){
						deffered.resolve(res);
					}).error(function(err){
						deffered.resolve(err);
					});
				return deffered.promise;
			}
		}
	});