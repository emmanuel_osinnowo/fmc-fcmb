var gcm = require('node-gcm');
var message = new gcm.Message();
 
//API Server Key
var sender = new gcm.Sender('AIzaSyA3LwF8WmV3BZLwVHNtf0hKtqFD_98u7tc');
var registrationIds = [];
 
// Value the payload data to send...
message.addData('message',"\u270C Peace, Love \u2764 and PhoneGap \u2706!");
message.addData('title','Push Notification Sample' );
message.addData('msgcnt','3'); // Shows up in the notification in the status bar
message.addData('soundname','beep.wav'); //Sound to play upon notification receipt - put in the www folder in app
//message.collapseKey = 'demo';
//message.delayWhileIdle = true; //Default is false
message.timeToLive = 3000;// Duration in seconds to hold in GCM and retry before timing out. Default 4 weeks (2,419,200 seconds) if not specified.
 
// At least one reg id required
registrationIds.push('APA91bEHkpRn6Gf4R7v9IcZsnsmYE9NxIOHIg30_rlYSL1_0YfDtH3CBCnL-BYjlZcnijeIJ3a-Ag5otOqMClqotKcv6ElF8Rsb3Kruu5xFliiJTN0LKnaf56g2_yZLUZTscmAWP1_j-5sipm3ojWw0GKucT9T027w');
 
/**
 * Parameters: message-literal, registrationIds-array, No. of retries, callback-function
 */
sender.send(message, registrationIds, 4, function (err, result) {
    console.log(result);
});